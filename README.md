------------------------------------------------------------------------------------------
TASK 1: Solve the logic puzzle at http://www.logic-puzzles.org/pdf/Z593AO.pdf using Prolog.
-------------------------------------------------------------------------------------------
TASK 2: Solve the logic puzzle at http://www.logic-puzzles.org/pdf/J161PC.pdf using Prolog.
-------------------------------------------------------------------------------------------
Instructions for compiling and running this Prolog file

After downloading all the files in this project and storing them in your directory, there are 2 ways to compile and run:


1. Using git bash:
. Log in to the directory by command: cd PHUONG_HAI_NGUYEN_PROLOGIII
. Type this command to log in to SWI-Prolog : swipl
. Test if the erl file has no error existing: ['FILENAME']. (Ex: ['index'].)
. Compile and run each function by command: FUNCTION_NAME(arg). (Ex: task1(Ashlyn, Mckenna, Asher, Tony, Casey). then type ; to see next possible results.)
-> the solutions for all functions will show up as the example below

2. Using PowerShell in Windows system
. Log in to the directory by command that contains files: cd /d YOURDIRECTORY (Ex: cd \d C:\Windows)
. Type this command to log in to SWI-Prolog: swipl
. Test if the erl file has no error existing: ['FILENAME']. (Ex: ['index'].)
. Compile and run each function by command: FUNCTION_NAME(arg,arg). (Ex: task2(Sawyer, Donald, Bryce, Jane, Walter). then type ; to see next possible results.)
-> the solutions for all functions will show up as the example below



                            Example Solutions for all functions
                            
//                          OPENING WINDOWS POWERSHELL  
// =============================================================================
Windows PowerShell
Copyright (C) 2014 Microsoft Corporation. All rights reserved.

PS C:\Users\ADMIN> cd .\PHUONG_HAI_NGUYEN_PROLOGIII
PS C:\Users\ADMIN\PHUONG_HAI_NGUYEN_PROLOGIII> swipl
Welcome to SWI-Prolog (threaded, 64 bits, version 7.6.0)
SWI-Prolog comes with ABSOLUTELY NO WARRANTY. This is free software.
Please run ?- license. for legal details.

For online help and background, visit http://www.swi-prolog.org
For built-in help, use ?- help(Topic). or ?- apropos(Word).

%%               Executing index.pl file
%%===============================================
1 ?- ['index'].
true.

%%               Task #1 Solution
%%===============================================
2 ?- task1(Ashlyn, Mckenna, Asher, Tony, Casey).
Ashlyn = [4, norwegian, avila],
Mckenna = [8, palauan, vaughan],
Asher = [9, argentinean, gillespie],
Tony = [10, zimbabwean, mercado],
Casey = [18, jordanian, rosa] ;
false.

%%               Task #2 Solution
%%===============================================
3 ?- task2(Sawyer, Donald, Bryce, Jane, Walter).
Sawyer = [220, will_it_blend, dining_table],
Donald = [515, rickrolling, juice_press],
Bryce = [531, dancing_baby, microwave],
Jane = [623, lolcat, blender],
Walter = [829, exploding_whale, coffee_maker] ;
false.

elements([Head],Holder) :- member(Head,Holder).
elements([Head|Tail],Holder) :- member(Head,Holder), elements(Tail, Holder).
 
%% 							Task 1
%%  Solve the logic puzzle at http://www.logic-puzzles.org/pdf/Z593AO.pdf using Prolog.
%%====================================================================================
task1(Ashlyn, Mckenna, Asher, Tony, Casey) :-

	Asher = [AsherPLACE, AsherPENPAL, AsherLAST],
	Ashlyn = [AshlynPLACE, AshlynPENPAL, AshlynLAST],
	Casey  = [CaseyPLACE, CaseyPENPAL, CaseyLAST],
	Mckenna  = [MckennaPLACE, MckennaPENPAL, MckennaLAST],
	Tony = [TonyPLACE, TonyPENPAL, TonyLAST],
	LogicPuzzles = [Asher, Ashlyn, Casey, Mckenna, Tony],

	elements([4,8,9,10,18], [AsherPLACE, AshlynPLACE, CaseyPLACE, MckennaPLACE, TonyPLACE]),
	elements([argentinean, jordanian, norwegian, palauan, zimbabwean], [AsherPENPAL, AshlynPENPAL, CaseyPENPAL, MckennaPENPAL, TonyPENPAL]),
	elements([avila, gillespie, mercado, rosa, vaughan], [AsherLAST, AshlynLAST, CaseyLAST, MckennaLAST, TonyLAST]),

	%% 1. The person in fourth place has Avila as a surname.
	member([4,_,avila], LogicPuzzles),

	%% 2. The person whose last name is Gillespie doesn't have the Norwegian penpal
	\+ member([_,norwegian,gillespie], LogicPuzzles),
	
	%% 3. The 5 elements were  the person whose last name is Mercado, the person with the Palauan penpal, the person in
	%% eighteenth place, Asher,  and the person whose last name is Avila
	\+ member([18,AsherPENPAL,AsherLAST],LogicPuzzles),
	\+ member([AsherPLACE,AsherPENPAL,mercado],LogicPuzzles),
	\+ member([AsherPLACE,palauan,AsherLAST],LogicPuzzles),
	\+ member([AsherPLACE,AsherPENPAL,avila],LogicPuzzles),
	\+ member([18,_,mercado],LogicPuzzles),
	\+ member([18,palauan,_],LogicPuzzles),
	\+ member([18,_,avila],LogicPuzzles),
	\+ member([_,palauan,mercado],LogicPuzzles),
	\+ member([_,palauan,avila],LogicPuzzles),

	%% 4. The person whose last name is Rosa doesn't have the Argentinean penpal.
	\+ member([_,argentinean,rosa], LogicPuzzles),
	
	%% 5. The person whose last name is Mercado finished after the person with the Argentinean penpal.
	member([X5PLACE,_,mercado], LogicPuzzles),
	member([Y5PLACE, argentinean, _], LogicPuzzles),
	X5PLACE > Y5PLACE,

	%% 6. Mckenna finished before the person whose last name is Merca
	member([MckennaPLACE,MckennaPENPAL,MckennaLAST], LogicPuzzles),
	member([X6PLACE,_,mercado], LogicPuzzles),
	MckennaPLACE < X6PLACE,

	%% 7. Either the person whose last name is Avila or the person whose last name is Gillespie has the Norwegian penpal.
	\+ member([_,norwegian,mercado], LogicPuzzles),
	\+ member([_,norwegian,rosa], LogicPuzzles),
	\+ member([_,norwegian,vaughan], LogicPuzzles),

	%% 8. The person in ninth place does not have Vaughan as a last name
	\+ member([9,_,vaughan], LogicPuzzles),

	%% 9. The person with the Jordanian penpal is Casey
	member([CaseyPLACE,jordanian,CaseyLAST], LogicPuzzles),

	%% 10. Ashlyn finished before the person with the Palauan penpal.
	member([AshlynPLACE,AshlynPENPAL,AshlynLAST], LogicPuzzles),
	member([X10PLACE,palauan,_], LogicPuzzles),
	AshlynPLACE < X10PLACE,

	%% 11. Of Tony and Mckenna, one came in tenth place and the other has Vaughan as a surname
	member([10,TonyPENPAL,TonyLAST], LogicPuzzles),
	member([MckennaPLACE,MckennaPENPAL,vaughan], LogicPuzzles).



%% 							Task 2
%% Solve the logic puzzle at http://www.logic-puzzles.org/pdf/J161PC.pdf using Prolog.
%%===================================================================================
task2(Sawyer, Donald, Bryce, Jane, Walter) :-

	Bryce = [BryceDATE, BryceCRAZE, BryceGIFT],
	Donald = [DonaldDATE, DonaldCRAZE, DonaldGIFT],
	Jane = [JaneDATE, JaneCRAZE, JaneGIFT],
	Sawyer = [SawyerDATE, SawyerCRAZE, SawyerGIFT],
	Walter = [WalterDATE, WalterCRAZE, WalterGIFT],
	LogicPuzzles = [Bryce, Donald, Jane, Sawyer, Walter],

	elements([220, 515, 531, 623, 829], [BryceDATE, DonaldDATE, JaneDATE, SawyerDATE, WalterDATE]),
	elements([dancing_baby, exploding_whale, lolcat, rickrolling, will_it_blend], [BryceCRAZE, DonaldCRAZE, JaneCRAZE, SawyerCRAZE, WalterCRAZE]),
	elements([blender, coffee_maker, dining_table, juice_press, microwave], [BryceGIFT, DonaldGIFT, JaneGIFT, SawyerGIFT, WalterGIFT]),

	%% 1. Bryce went on vacation after Sawyer.
	member([BryceDATE,_,_], LogicPuzzles),
	member([SawyerDATE,_,_], LogicPuzzles),
	BryceDATE > SawyerDATE,

	%% 2. The 5 elements were  the person who started the rickrolling craze, the vacationer who left on May 31, the person who
	%% received the dining table, Walter,  and the person who started the lolcat craze.
	\+ member([531,WalterCRAZE,WalterGIFT],LogicPuzzles),
	\+ member([WalterDATE,rickrolling,WalterGIFT],LogicPuzzles),
	\+ member([WalterDATE,lolcat,WalterGIFT],LogicPuzzles),
	\+ member([WalterDATE,WalterCRAZE,dining_table],LogicPuzzles),
	\+ member([531,rickrolling,_],LogicPuzzles),
	\+ member([531,_,dining_table],LogicPuzzles),
	\+ member([531,lolcat,_],LogicPuzzles),
	\+ member([_,lolcat,dining_table],LogicPuzzles),
	\+ member([_,rickrolling,dining_table],LogicPuzzles),

	%% 3. The person who received the microwave is Bryce
	member([BryceDATE, BryceCRAZE, microwave], LogicPuzzles),

	%% 4. Jane went on vacation after the person who received the juice press.
	member([JaneDATE,_,_], LogicPuzzles),
	member([X4DATE,_,juice_press], LogicPuzzles),
	JaneDATE > X4DATE,

	%% 5. Of Walter and the person who started the dancing baby craze, one left for vacation on August 29 and the other loved
	%% the microwave they received.
	member([829,WalterCRAZE,WalterGIFT], LogicPuzzles),
	member([_,dancing_baby,microwave], LogicPuzzles),
	\+ member([829,_,microwave],LogicPuzzles),

	%% 6. The person who started the exploding whale craze is Walter
	member([WalterDATE, exploding_whale, WalterGIFT], LogicPuzzles),

	%% 7. The person who received the juice press went on vacation before the person who received the coffee maker
	member([X7,_,juice_press], LogicPuzzles),
	member([Y7,_,coffee_maker], LogicPuzzles),
	X7 < Y7,

	%% 8. Either the vacationer who left on February 20 or the vacationer who left on August 29 started the will it blend craze.
	\+ member([515,will_it_blend,_],LogicPuzzles),
	\+ member([531,will_it_blend,_],LogicPuzzles),
	\+ member([623,will_it_blend,_],LogicPuzzles),

	%% 9. The person who received the juice press didn't start the lolcat craze.
	\+ member([_,lolcat, juice_press], LogicPuzzles),
	
	%% 10. The person who received the coffee maker is not Jane
	\+ member([JaneDATE,JaneCRAZE,coffee_maker], LogicPuzzles),

	%% 11. The person who started the rickrolling craze is not Sawyer.
	\+ member([SawyerDATE,rickrolling ,SawyerGIFT], LogicPuzzles).
